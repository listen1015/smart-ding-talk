# smart-ding-talk

>一款精简的发送钉钉机器人消息的工具

### maven依赖：
```$xslt

        <dependency>
            <groupId>net.1024lab</groupId>
            <artifactId>smart-ding-talk</artifactId>
            <version>1.0.0-SNAPSHOT</version>
        </dependency>

```

### 钉钉群自定义机器人消息推送示例：

***spring-boot ：***

```$xslt

@Configuration
public class SmartDingTalkConfig {

    @Bean
    public DingTalkRobotNotify createDingTalkRobotNotify() {
        DingTalkRobotNotify robotNotify = DingTalkRobotNotifyBuilder.custom()
            // 如果需要自定义HttpClient 可以手动指定, 否则不需要
            .setHttpClient()
            // 钉钉机器人 access token
            .setRobotAccessToken("xxxxx")
            // 设置 secret
            .setSecret("xxxx")
            .build();
        return robotNotify;
    }
}

```

***机器人消息推送示例：***
```$xslt

    @Autowired
    private DingTalkRobotNotify dingTalkRobotNotify;
    
    public void test() {
        DingTalkRobotMsgMarkdown msg = new DingTalkRobotMsgMarkdown();
        msg.setTitle("test title");
        msg.setText("the little yellow flower is floating in the air since the day of my birth");
        dingTalkRobotNotify.sendNotify(msg);
    }

```

***目前支持的机器人消息格式:***

格式|class
---|---
markdown|DingTalkRobotMsgMarkdown
text|DingTalkRobotMsgText


