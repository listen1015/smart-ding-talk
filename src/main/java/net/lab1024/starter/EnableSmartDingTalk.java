package net.lab1024.starter;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 启用 标记注解
 *
 * @author listen
 * @date 2020年9月20日 15:22:09
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(DingTalkRobotConfiguration.class)
public @interface EnableSmartDingTalk {

}
