package net.lab1024.starter;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 配置类
 *
 * @author Turbolisten
 * @date 2020/9/20 14:49
 */
@ConfigurationProperties(prefix = DingTalkRobotConfig.CONFIG_PREFIX)
public class DingTalkRobotConfig {

    protected static final String CONFIG_PREFIX = "smart-ding-talk.robot";

    /**
     * 机器人 url
     */
    private String url = "https://oapi.dingtalk.com/robot/send?access_token=";

    /**
     * token
     */
    private String accessToken;

    /**
     * 密钥
     */
    private String secret;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    @Override
    public String toString() {
        return "DingTalkRobotConfig{" +
                "url='" + url + '\'' +
                ", accessToken='" + accessToken + '\'' +
                ", secret='" + secret + '\'' +
                '}';
    }
}
