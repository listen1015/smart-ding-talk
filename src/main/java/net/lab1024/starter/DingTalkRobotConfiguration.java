package net.lab1024.starter;


import net.lab1024.robot.DingTalkRobotNotify;
import net.lab1024.robot.DingTalkRobotNotifyBuilder;
import net.lab1024.util.StringUtil;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 钉钉机器人 配置类
 *
 * @author Turbolisten
 * @date 2020/9/20 14:49
 */
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(DingTalkRobotConfig.class)
public class DingTalkRobotConfiguration {

    @Bean
    public DingTalkRobotNotify createDingTalkRobotNotify(DingTalkRobotConfig robotConfig) {

        System.out.println("--------------- create DingTalkRobotNotify robotConfig ------------------ " + robotConfig.toString());

        if (StringUtil.isBlank(robotConfig.getAccessToken()) || StringUtil.isBlank(robotConfig.getSecret())) {
            throw new RuntimeException("SmartDingTalkRobot init failed : accessToken and secret not blank");
        }

        return DingTalkRobotNotifyBuilder.custom()
                // 如果需要自定义HttpClient 可以手动指定, 否则不需要     .setHttpClient()
                // 钉钉机器人 access token
                .setRobotAccessToken(robotConfig.getAccessToken())
                // 设置 secret
                .setSecret(robotConfig.getSecret())
                .setDingTalkUrl(robotConfig.getUrl())
                .build();
    }
}
