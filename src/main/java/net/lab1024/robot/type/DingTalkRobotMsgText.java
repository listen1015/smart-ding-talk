package net.lab1024.robot.type;
/**
 * 钉钉推送消息 text 类型
 *
 * @author listen
 * @date 2019/07/16 17:04
 */
public class DingTalkRobotMsgText extends DingTalkRobotMsg {

    /**
     * 标题
     */
    private String content;

    public DingTalkRobotMsgText() {
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * 获取消息类型
     *
     * @return
     */
    @Override
    public DingTalkRobotMsgTypeEnum getMsgType() {
        return DingTalkRobotMsgTypeEnum.TEXT;
    }
}
