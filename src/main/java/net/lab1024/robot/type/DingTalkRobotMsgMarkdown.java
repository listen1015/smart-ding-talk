package net.lab1024.robot.type;

import net.lab1024.robot.type.DingTalkRobotMsg;
import net.lab1024.robot.type.DingTalkRobotMsgTypeEnum;

/**
 * 钉钉推送消息 markdown 类型
 *
 * @author listen
 * @date 2019/07/16 17:04
 */
public class DingTalkRobotMsgMarkdown extends DingTalkRobotMsg {

    /**
     * 标题
     */
    private String title;

    /**
     * markdown 内容
     */
    private String text;

    public DingTalkRobotMsgMarkdown() {
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }
    /**
     * 获取消息类型
     *
     * @return
     */
    @Override
    public DingTalkRobotMsgTypeEnum getMsgType() {
        return DingTalkRobotMsgTypeEnum.MARKDOWN;
    }
}
