package net.lab1024.robot.type;
/**
 * 钉钉消息推送类型枚举类
 *
 * @author listen
 * @date 2019/07/16 16:19
 */
public enum DingTalkRobotMsgTypeEnum {

    /**
     * 文本类型
     */
    TEXT,

    /**
     * MARKDOWN 类型
     */
    MARKDOWN

}
