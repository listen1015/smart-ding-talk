package net.lab1024.robot.type;


/**
 * 钉钉推送消息基类
 *
 * @author listen
 * @date 2019/07/16 17:06
 */
public abstract class DingTalkRobotMsg {

    private Boolean atAll;

    /**
     * 获取消息类型
     *
     * @return
     */
    public abstract DingTalkRobotMsgTypeEnum getMsgType();

    public Boolean getAtAll() {
        return atAll;
    }
    public void setAtAll(Boolean atAll) {
        this.atAll = atAll;
    }
}
