package net.lab1024.util;
/**
 * String 工具类
 *
 * @author listen
 * @date 2019/07/16 17:42
 */
public class StringUtil {

    /**
     * 判断字符串是否为空
     *
     * @param str
     * @return
     */
    public static boolean isBlank(String str) {
        return null == str || str.trim().length() == 0;
    }

    /**
     * 判断字符串是否不为空
     *
     * @param str
     * @return
     */
    public static boolean isNotBlank(String str) {
        return ! isBlank(str);
    }

}
